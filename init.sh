#!/bin/bash

source functions.sh
source globals.sh
source ${PROPERTY_FILE}
#limit=$(($limit*iterator))
i=1
n=$(($RampUpUser))
#Get the limit value
while [ $i -le $((limit-1)) ]
         do
           if [ "${operation}" = "multiply" ]
              then 
	             n=$((n*iterator ))
	          else
	             n=$((n+iterator ))
           fi	 
	       i=$((i+1))
       done
       n=$((n+1))
    echo "limit : ${n}"
 #Get the Page Name to be tested from the Page Name Array

 for entry in "$TestsConfigDir"/*
 do
   if [[ ${entry} =~ .*\.yml$ ]];then
    ArrivalCount=${RampUpUser}
    ScenarioName="${entry:15:-4}"
    priority=${ScenarioName%_*}
    ScenarioName=${ScenarioName#*_}
    echo "#################################################################################################################################"
    echo "Scenario : ${ScenarioName}"
    echo "ArrivalCount : ${ArrivalCount}"
    echo "priority : ${priority}"
    
    docittnafUrl=${docittnafUrl}
    docittqaurl=${docittqaurl}
    echo "docittnafUrl : $docittnafUrl"
    echo "docittqaurl : $docittqaurl"
    echo "Token : Bearer $token"
    echo  -e "#################################################################################################################################\n\n"
     #compares the Arrival Rate with the Limit Value From the Property File
      while [  $((ArrivalCount)) -lt ${n} ]
      do
       #start the run
       if [ $((priority)) -gt 10 ]
         then
         StartTime=$(timestamp)
         echo "StarTime : ${StartTime}"
 
#       sudo chmod 777 $OutputDir/${ScenarioName}_${ArrivalCount}.txt
#       sudo chmod 777 $DesginationData/EmailID_${ArrivalCount}.csv
         
         sleep 2s
         CSVFilePath=$DesginationData/EmailID_${ArrivalCount}.csv
         FieldName1=Email
         FieldName2=ID1
         FieldName3=ID2
         DEBUG=http* artillery -v '{"docittnafURL":"'${docittnafUrl}'","docittURL":"'${docittqaurl}'","Borrowerservice":"'${Borrowerservice}'","AppStore":"'${AppStore}'"}' run --overrides '{"config": {"phases": [{"duration": '${duration}',"arrivalCount": '${ArrivalCount}'}],"defaults": {"headers": {"Authorization": "Bearer '${token}'"}},"payload": {"path": "'${CSVFilePath}'","fields": ["'${FieldName1}'","'${FieldName2}'","'${FieldName3}'"],"order": "sequence"}}}' -o ${OutputDir}/${ScenarioName}_${ArrivalCount}.json ${entry} 2>&1 | tee -a $OutputDir/${ScenarioName}_${ArrivalCount}.log.txt 
         else
         StartTime=$(timestamp)
         echo "StarTime : ${StartTime}"
#        sudo chmod 777 $OutputDir/${ScenarioName}_${ArrivalCount}.txt
#       sudo chmod 777 $DesginationData/EmailID_${ArrivalCount}.csv
         
         sleep 2s
         DEBUG=http* artillery -v '{"docittnafURL":"'${docittnafUrl}'","docittURL":"'${docittqaurl}'","Borrowerservice":"'${Borrowerservice}'","AppStore":"'${AppStore}'"}' run --overrides '{"config": {"phases": [{"duration": '${duration}',"arrivalCount": '${ArrivalCount}'}],"defaults": {"headers": {"Authorization": "Bearer '${token}'"}}}}' -o ${OutputDir}/${ScenarioName}_${ArrivalCount}.json ${entry} 2>&1 | tee -a $OutputDir/${ScenarioName}_${ArrivalCount}.log.txt
       fi
         sleep 2s
         #Get the values and compute the aggregate from the logs JSON
         Aggregate=$(getAggregater ${OutputDir}/${ScenarioName}_${ArrivalCount}.json)
         echo "Aggregate : "${Aggregate}
          if [ $((priority)) -eq 10 ]
           then
             reNameFile ${ArrivalCount}
           else
             echo "priority : ${priority}"
         fi
         #Checks the Aggregate against the Benchmark from Prop File and decides to increase the Arrival Rate or Stops the Execution
         if [[ $(bc <<< "${Aggregate}>=$((benchmark))") -eq 1 ]]
          then
            StopTime=$(timestamp)
            echo "EndTime : ${StopTime}"
            echo "#################################################################################################################################" >> $OutputDir/${ScenarioName}_${ArrivalCount}.txt
            
            generateJSON ${ScenarioName} ${ArrivalCount} ${Aggregate} ${OutputDir}/${ScenarioName}_${ArrivalCount}.json ${StartTime} ${StopTime} ${ScenarioName}_${ArrivalCount}.txt
            if [ "${operation}" = "multiply" ]
             then
               ArrivalCount=$((ArrivalCount*iterator))
             else
               ArrivalCount=$((ArrivalCount+iterator))
            fi
            echo "New Arrival Rate : "${ArrivalCount}
          else
            StopTime=$(timestamp)
            echo "New Arrival Rate : "${ArrivalCount}
            echo "EndTime : ${StopTime}"
            
            generateJSON ${ScenarioName} ${ArrivalCount} ${Aggregate} ${OutputDir}/${ScenarioName}_${ArrivalCount}.json ${StartTime} ${StopTime} ${ScenarioName}_${ArrivalCount}.txt
            ArrivalCount=${n}
            sleep 15s
        fi
      done
    fi
 done

generateReport $(timestamp) ${duration}
sleep 3s

echo "############## Trigger Email #####################################"
sudo chmod 777 Email.sh
./Email.sh


#!/bin/bash

ReportLoc="./report_files"
PEMDesginationFolder="./report_files/docitt-dev.pem"
MongoDBPEMDesginationFolder="./report_files/docitt-mongo.pem"
FolderName=$(date +%Y%m%d)
mkdir -p "./output"/${FolderName}/
mkdir -p "./test_data"/${FolderName}/
OutputDir="./output"/${FolderName}
SourceTestData="./test_data"/${FolderName}
DesginationData="../test_data"/${FolderName}
ReportFile=${OutputDir}/"Report.html"
PROPERTY_FILE="./Devenv.properties"
TestsConfigDir="./test_scripts"
head=`cat ${ReportLoc}/head.txt`
rest=`cat ${ReportLoc}/rest.txt`
NewJson='{"timestamp":"_","duration":"_","result":[]}'

#!/bin/bash
source functions.sh
source globals.sh
source ${PROPERTY_FILE}

sudo ssh ${HostServerUsername}@${HostIPAddress} -i $PEMDesginationFolder << EOF
#sleep 45
free -m
docker ps -q | xargs  docker stats --no-stream
EOF

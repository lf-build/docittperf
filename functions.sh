#!/bin/bash

source globals.sh

function getProperty {
   PROP_VALUE=`cat ${PROPERTY_FILE} | grep "$1" | cut -d'=' -f2`
   echo ${PROP_VALUE}
}

function getAggregater {
 ScenarioCount=`jq '.aggregate.scenariosCompleted' $1`
 ScenarioCreated=`jq '.aggregate.scenariosCreated' $1`
 node -pe $((ScenarioCount))/$((ScenarioCreated))*100
}
function setColumnName {
  echo "Email" >> EmailID_.csv;
}
function reNameFile {
  touch EmailID_.csv
  mv EmailID_.csv $SourceTestData/EmailID_${ArrivalCount}.csv
}

function serverMonitoring {
   echo "Email" >> $OutputDir/$1
}

function generateReport {
 NewJson=`echo ${NewJson} | jq '.timestamp="'$1" "$2'"' |jq '.duration="'$3'"'`
 echo ${head} > ${OutputDir}/Report.html
 echo "window.response=$NewJson;" >> ${OutputDir}/Report.html
 echo "${rest}" >> ${OutputDir}/Report.html
 sleep 2s
 #google-chrome ${OutputDir}/"Report.html"
 firefox ${OutputDir}/"Report.html"
 generateIndividualReport
}

function generateIndividualReport {
 check=$(getProperty "generateIndividualReports")
 if ${check}; then
   for entry in "$OutputDir"/*
   do
    if [[ $entry =~ .*\.json$ ]];then
        artillery report "$entry"
    fi
   done
 fi
}

function getCodes {
  if [ ${1%.*} -eq "0" ]
  then
      newCodes=`jq '.aggregate.codes' $2`
      echo $newCodes >> tempA.json
      erros=`jq '.aggregate.errors' $2`>> tempB.json
      echo $erros >> tempB.json
      tempJson=`jq -n --arg startTime "$3" --arg stopTime "$4"  '{"StartTime": $startTime, "EndTime": $stopTime}'`
      echo ${tempJson}  >> tempC.json
      codes=`jq -s add ./tempA.json ./tempB.json ./tempC.json`
      rm -r ./tempA.json ./tempB.json ./tempC.json
  else
      newCodes=`jq '.aggregate.codes' $2`
      echo $newCodes >> tempA.json
      scnd=`jq '.aggregate.scenarioDuration' $2`
      echo $scnd >> tempB.json
      erros=`jq '.aggregate.errors' $2`>> tempD.json
      echo $erros >> tempD.json
      tempJson=`jq -n --arg startTime "$3" --arg stopTime "$4"  '{"StartTime": $startTime, "EndTime": $stopTime}'`
      echo ${tempJson}  >> tempC.json
      codes=`jq -s add ./tempA.json ./tempB.json ./tempD.json ./tempC.json`
      rm -r ./tempA.json ./tempB.json ./tempD.json ./tempC.json
  fi
  echo ${codes}
  }

function generateJSON {
 sampleJson=`jq -n --arg scenario "$1" --arg filename "$9" --arg aggregate "$3" --arg arrivalrate "$2" --argjson err "$(getCodes "$3" "$4" ""$5" "$6"" ""$7" "$8"")" '{"ScenarioName": $scenario, "FileName": $filename, "Aggregate": $aggregate, "ArrivalRate": $arrivalrate, "codes" : $err}'`
 echo ${sampleJson}
 NewJson=`echo ${NewJson}| jq ".result[.result|length] |= .+ ${sampleJson}"`
 echo ${NewJson}
}

timestamp() {
  echo $(date +"%Y-%m-%d %T")
}



// @ts-nocheck
var fs = require('fs');
var id = 0;


module.exports = {
    generateLoanNumber: generateLoanNumber,
    generateUserName:generateUserName,
    generateEmail:generateEmail,
    setHeaderSecurityIdentity:setHeaderSecurityIdentity,
    generateEmailAndWrite:generateEmailAndWrite,
    calculateResponseTimeDelta:calculateResponseTimeDelta,
    createGUID1:createGUID1,
    createGUID2:createGUID2,
    setHeaderMultiPart:setHeaderMultiPart,
    UploadDocMultipart:UploadDocMultipart,
    createGUID3:createGUID3
}

function generateLoanNumber(requestParams, context, ee, next) {
    id += 1;
    var DynamicNumber = '0000000' + id;
    context.vars['DynamicNumber'] = '15' + DynamicNumber.slice(-7);
    return next();
}

function generateEmail(requestParams, context, ee, next) {
   var text = "";
   var text2 = "";
   var text3 = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 11; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    text2 += possible.charAt(Math.floor(Math.random() * possible.length));
    text3 += possible.charAt(Math.floor(Math.random() * possible.length));
   }
    context.vars['Email'] = 'rengarajan.sigmainfosoln+' + text + text2 + text3 + "@gmail.com"
    return next();
}

function generateEmailAndWrite(requestParams, context, ee, next) {
   var text = "";
   var text2 = "";
   var text3 = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 11; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    text2 += possible.charAt(Math.floor(Math.random() * possible.length));
    text3 += possible.charAt(Math.floor(Math.random() * possible.length));
   }
   context.vars['Email'] = 'rengarajan.sigmainfosoln+' + text + text2 + text3 + "@gmail.com"

   var FilePath = 'EmailID_'+'.csv'
   fs.appendFileSync(FilePath, context.vars['Email']);
   fs.appendFileSync(FilePath, ',');
   return next();
}

function generateUserName(requestParams, context, ee, next) {
   var text = "";
   var text2 = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 4; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    text2 += possible.charAt(Math.floor(Math.random() * possible.length));
   }
   context.vars['UserName'] = 'User_' + text + text2
   return next();
}

function setHeaderSecurityIdentity(requestParams, context, ee, next){
  requestParams.headers.authorization="Bearer " + context.vars['LoginToken'];
  return next();
}

function setHeaderMultiPart(requestParams, context, ee, next){
  requestParams.headers.ContentType="multipart/form-data; boundary=---------------------------6019050455527684232074285292";;
  return next();
}

function calculateResponseTimeDelta(requestSpec, response, context, ee) {
  // requestSpec will be the request spec for this response (currently always null)
  // response is a Request.js response object
  // context is the scenario context containing scenario variables
  // ee is an event emitter for this scenario that we can use to add custom stats to the report
  
  var responseTime = Number(response.headers['x-response-time'].split('ms')[0]);
    
  ee.emit('customStat', { stat: 'response_time', value: responseTime });
  return next();
}
 
function createGUID1(requestParams, context, ee, next) {  
   function s4() {  
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);  
   }
   context.vars['ID1'] = "ID-" + s4() + s4() + '-' + s4() + '-' +s4() + '-' + s4() + '-' + s4() + s4() + s4();
   var FilePath = 'EmailID_'+'.csv'
   fs.appendFileSync(FilePath, context.vars['ID1']);
   fs.appendFileSync(FilePath, ',');
   return next();
}

function createGUID2(requestParams, context, ee, next) {  
   function s4() {  
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);  
   }
   context.vars['ID2'] = "ID-"+ s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
   var FilePath = 'EmailID_'+'.csv'
   fs.appendFileSync(FilePath, context.vars['ID2']);
   fs.appendFileSync(FilePath, ',');
   return next();
}

function createGUID3(requestParams, context, ee, next) {  
	   function s4() {  
	      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);  
	   }
	   context.vars['ID3'] = "ID-"+ s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	   var FilePath = 'EmailID_'+'.csv'
	   fs.appendFileSync(FilePath, context.vars['ID3']);
	   fs.appendFileSync(FilePath, '\n');
	   return next();
	}

function UploadDocMultipart(requestParams, context, ee, next){

	var Auth = 'Bearer ' + context.vars['LoginToken']
	var Request = '{"requestId":"'+ context.vars['DocumentID'] + '","createdby":"' + context.vars['Email'] + '"}'
	var url = 'http://docitt.qa.lendfoundry.com:9002/required-condition/application' + context.vars['ApplicationNumber'] +  '/' + context.vars['DocumentID'] + '/submit';
	var headers = {
	   'Authorization': Auth
	};

	var readStream = fs.createReadStream('/var/lib/jenkins/workspace/DocittPerf/files/OtherIncome.jpg');
	readStream.on('error', function (e) {
	     console.log("Some error with the file"+e);
	});
	readStream.on('open', function () {
	     console.log("File has been Read");
	});

	var FormData = { file: readStream , metaData: Request, explanation: 'Test'};
	var jsonObject;
	request.post({ url: url, formData : FormData, headers: headers }, function (err, httpResponse, body) {
		if (err) {
			return console.error('upload failed:', err);
			}
			console.log('Upload successful!  Server responded with:', body);

			});

	}
